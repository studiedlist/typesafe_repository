use crate::prelude::*;
use std::marker::PhantomData;
use tokio::sync::Mutex;

/// Example usage of [Repository](super::Repository) trait
///
/// Note that neither efficiency in performance nor memory are design goals of
/// `InMemoryRepository`.
/// Its main purpose is to mock any repository for tests
///
/// Here is an example of how to use `InMemoryRepository` to store your struct
/// ```
/// use typesafe_repository::inmemory::InMemoryRepository;
/// use typesafe_repository::{Identity, GetIdentity, RefIdentity};
/// use typesafe_repository::macros::Id;
/// use std::error::Error;
///
/// #[derive(Id, Clone, Debug, PartialEq)]
/// #[Id(get_id, ref_id)]
/// struct MyStruct {
///     #[id]
///     name: String,
///     data: usize,
/// }
///
/// #[tokio::main]
/// async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
///     let repository = InMemoryRepository::<_, Box<dyn Error + Send + Sync>>::new();
///     let st = MyStruct { name: String::from("abc"), data: 123 };
///
///     // Perform some async operations
///     {
///         use typesafe_repository::async_ops::*;
///
///         repository.add(st.clone()).await?;
///         let result = repository.get_one(st.id_ref()).await?;
///         assert_eq!(result, Some(st.clone()));
///
///         let res = repository.map(
///             st.id_ref(),
///             |st| Ok(MyStruct { data: 321, ..st }),
///         ).await?;
///
///         assert!(res.is_some());
///
///         let result = repository.get_one(st.id_ref()).await?;
///         assert_eq!(result, Some(MyStruct { data: 321, ..st.clone() }));
///
///         repository.remove(st.id_ref()).await?;
///         let result = repository.get_one(st.id_ref()).await?;
///         assert!(result.is_none());
///     }
///
///     // Perform some sync operations as well
///     tokio::task::block_in_place(move || {
///         use typesafe_repository::ops::*;
///
///         repository.add(st.clone())?;
///         let result = repository.get_one(st.id_ref())?;
///         assert_eq!(result, Some(st.clone()));
///
///         repository.remove(st.id_ref())?;
///         let result = repository.get_one(st.id_ref())?;
///         assert!(result.is_none());
///         Ok::<(), Box<dyn Error + Send + Sync>>(())
///     })?;
///
///     Ok(())
/// }
///
/// ```
pub struct InMemoryRepository<T: Identity, E> {
    cache: Mutex<Vec<(<T as Identity>::Id, T)>>,
    _e: PhantomData<E>,
}

impl<T: Identity, E> Default for InMemoryRepository<T, E> {
    fn default() -> Self {
        Self {
            cache: Mutex::new(vec![]),
            _e: PhantomData,
        }
    }
}

#[allow(clippy::new_without_default)]
impl<T: Identity, E> InMemoryRepository<T, E> {
    pub fn new() -> Self {
        Self {
            cache: Mutex::new(vec![]),
            _e: PhantomData,
        }
    }
}

impl<T, E> Repository<T> for InMemoryRepository<T, E>
where
    T: Identity + Clone,
    E: Send + 'static,
{
    type Error = E;
}

#[cfg(feature = "async")]
mod async_ops {
    use super::*;
    use crate::async_ops::*;
    use async_trait::async_trait;

    #[async_trait]
    impl<T, E> List<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Clone + Send + Sync,
        T::Id: Clone + Send + Sync,
    {
        async fn list(&self) -> Result<Vec<T>, Self::Error> {
            Ok(self
                .cache
                .lock()
                .await
                .iter()
                .cloned()
                .map(|(_, v)| v)
                .collect())
        }
    }

    #[async_trait]
    impl<T, E> Get<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + RefIdentity + Clone + Sync + Send,
        T::Id: Clone + Sync + Send + Eq,
    {
        async fn get_one(&self, k: &T::Id) -> Result<Option<T>, Self::Error> {
            Ok(self
                .cache
                .lock()
                .await
                .iter()
                .cloned()
                .map(|(_, v)| v)
                .find(|v| v.id_ref() == k))
        }
    }

    #[async_trait]
    impl<T, E> Save<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + RefIdentity + GetIdentity + Clone + Send + Sync + 'static,
        T::Id: Send + Sync,
        Self: Get<T> + Update<T>,
    {
        async fn save(&self, v: T) -> Result<(), Self::Error> {
            let id = v.id_ref();
            match self.get_one(id).await? {
                Some(_) => self.update(v).await?,
                None => self.add(v).await?,
            }
            Ok(())
        }
    }

    #[async_trait]
    impl<T, E> Update<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + RefIdentity + GetIdentity + Clone + Send + Sync,
        T::Id: Send + Sync,
        Self: Remove<T>,
    {
        async fn update(&self, t: T) -> Result<(), Self::Error> {
            self.remove(t.id_ref()).await?;
            self.add(t).await?;
            Ok(())
        }
    }

    #[async_trait]
    impl<T, K, E> Remove<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity<Id = K> + Clone + Sync + Send,
        K: Clone + Sync + Send + Eq,
    {
        async fn remove(&self, id: &K) -> Result<(), Self::Error> {
            let mut cache = self.cache.lock().await;
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (k, _))| k == id)
                .map(|(i, _)| i);
            let Some(i) = res else { return Ok(()) };
            cache.remove(i);
            Ok(())
        }
    }

    #[async_trait]
    impl<T, K, E> Take<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity<Id = K> + Clone + Sync + Send,
        K: Clone + Sync + Send + Eq,
    {
        async fn take(&self, id: &K) -> Result<Option<T>, Self::Error> {
            let mut cache = self.cache.lock().await;
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (k, _))| k == id)
                .map(|(i, (_, v))| (i, v.clone()));
            let Some((i, v)) = res else { return Ok(None) };
            cache.remove(i);
            Ok(Some(v))
        }
    }

    #[async_trait]
    impl<T, E> Add<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + GetIdentity + Clone + Send + Sync,
        T::Id: Send + Sync,
    {
        async fn add(&self, t: T) -> Result<(), Self::Error> {
            let mut cache = self.cache.lock().await;
            cache.push((t.id(), t));
            Ok(())
        }
    }

    #[async_trait]
    impl<T, E, S> ListBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Clone + Send + Sync,
        T::Id: Clone + Send + Sync,
        T: IdentityBy<S>,
        S: PartialEq + Send + Sync,
    {
        async fn list_by(&self, s: &S) -> Result<Vec<T>, Self::Error> {
            let cache = self.cache.lock().await;
            Ok(cache
                .iter()
                .cloned()
                .map(|(_, x)| x)
                .filter(|x| x.id_by() == *s)
                .collect())
        }
    }

    #[async_trait]
    impl<T, E, S> GetBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Clone + Send + Sync,
        T::Id: Send + Sync,
        T: IdentityBy<S>,
        S: PartialEq + Send + Sync,
    {
        async fn get_by(&self, s: &S) -> Result<Option<T>, Self::Error> {
            let cache = self.cache.lock().await;
            Ok(cache
                .iter()
                .map(|(_, x)| x)
                .find(|x| x.id_by() == *s)
                .cloned())
        }
    }

    #[async_trait]
    impl<T, E> Map<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + RefIdentity + GetIdentity + Send + Sync + Clone,
        T::Id: Clone + Send + Sync + Eq,
    {
        async fn map<F: Fn(T) -> Result<T, Self::Error> + Send>(
            &self,
            id: &T::Id,
            f: F,
        ) -> Result<Option<T>, Self::Error> {
            let t = self.get_one(id).await?;
            if let Some(t) = t {
                let t = f(t)?;
                self.update(t.clone()).await?;
                Ok(Some(t))
            } else {
                Ok(None)
            }
        }
    }

    #[async_trait]
    impl<T, E, S> RemoveBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Send + Sync + Clone,
        T::Id: Clone + Send + Sync + Eq,
        T: IdentityBy<S>,
        S: PartialEq + Send + Sync,
    {
        async fn remove_by(&self, id: &S) -> Result<(), Self::Error> {
            let mut cache = self.cache.lock().await;
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (_, v))| &v.id_by() == id)
                .map(|(i, _)| i);
            let Some(i) = res else { return Ok(()) };
            cache.remove(i);
            Ok(())
        }
    }

    #[async_trait]
    impl<T, E> Clear<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Send + Sync + Clone,
        T::Id: Send,
    {
        async fn clear(&self) -> Result<(), Self::Error> {
            let mut cache = self.cache.lock().await;
            *cache = vec![];
            Ok(())
        }
    }

    #[async_trait]
    impl<T, E> ListPaged<T> for InMemoryRepository<T, E>
    where
        E: Send + Sync + 'static,
        T: Identity + Send + Sync + Clone,
        T::Id: Send,
    {
        async fn list_paged(&self, size: usize, page: usize) -> Result<Vec<T>, Self::Error> {
            let cache = self.cache.lock().await;
            Ok(cache
                .iter()
                .skip(page.saturating_mul(size))
                .take(size)
                .map(|(_, v)| v)
                .cloned()
                .collect())
        }
    }
}

mod ops {
    use super::InMemoryRepository;
    use crate::ops::*;
    use crate::{GetIdentity, Identity, IdentityBy, RefIdentity};

    impl<T, E> List<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
        T::Id: Clone,
    {
        fn list(&self) -> Result<Vec<T>, Self::Error> {
            Ok(self
                .cache
                .blocking_lock()
                .iter()
                .cloned()
                .map(|(_, v)| v)
                .collect())
        }
    }

    impl<T, E> Get<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + RefIdentity + Clone,
        T::Id: Clone + Eq,
    {
        fn get_one(&self, k: &T::Id) -> Result<Option<T>, Self::Error> {
            Ok(self
                .cache
                .blocking_lock()
                .iter()
                .cloned()
                .map(|(_, v)| v)
                .find(|v| v.id_ref() == k))
        }
    }

    impl<T, E> Save<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + RefIdentity + GetIdentity + Clone + 'static,
        Self: Get<T> + Update<T>,
    {
        fn save(&self, v: T) -> Result<(), Self::Error> {
            let id = v.id_ref();
            match self.get_one(id)? {
                Some(_) => self.update(v)?,
                None => self.add(v)?,
            }
            Ok(())
        }
    }

    impl<T, E> Update<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + RefIdentity + GetIdentity + Clone,
        Self: Remove<T>,
    {
        fn update(&self, t: T) -> Result<(), Self::Error> {
            self.remove(t.id_ref())?;
            self.add(t)?;
            Ok(())
        }
    }

    impl<T, K, E> Remove<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity<Id = K> + Clone,
        K: Clone + Eq,
    {
        fn remove(&self, id: &K) -> Result<(), Self::Error> {
            let mut cache = self.cache.blocking_lock();
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (k, _))| k == id)
                .map(|(i, _)| i);
            let Some(i) = res else { return Ok(()) };
            cache.remove(i);
            Ok(())
        }
    }

    impl<T, K, E> Take<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity<Id = K> + Clone,
        K: Clone + Eq,
    {
        fn take(&self, id: &K) -> Result<Option<T>, Self::Error> {
            let mut cache = self.cache.blocking_lock();
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (k, _))| k == id)
                .map(|(i, (_, v))| (i, v.clone()));
            let Some((i, v)) = res else { return Ok(None) };
            cache.remove(i);
            Ok(Some(v))
        }
    }

    impl<T, E> Add<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + GetIdentity + Clone,
    {
        fn add(&self, t: T) -> Result<(), Self::Error> {
            let mut cache = self.cache.blocking_lock();
            cache.push((t.id(), t));
            Ok(())
        }
    }

    impl<T, E, S> ListBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
        T::Id: Clone,
        T: IdentityBy<S>,
        S: PartialEq,
    {
        fn list_by(&self, s: &S) -> Result<Vec<T>, Self::Error> {
            let cache = self.cache.blocking_lock();
            Ok(cache
                .iter()
                .cloned()
                .map(|(_, x)| x)
                .filter(|x| x.id_by() == *s)
                .collect())
        }
    }

    impl<T, E, S> GetBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
        T: IdentityBy<S>,
        S: PartialEq,
    {
        fn get_by(&self, s: &S) -> Result<Option<T>, Self::Error> {
            let cache = self.cache.blocking_lock();
            Ok(cache
                .iter()
                .map(|(_, x)| x)
                .find(|x| x.id_by() == *s)
                .cloned())
        }
    }

    impl<T, E> Map<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + RefIdentity + GetIdentity + Clone,
        T::Id: Clone + Eq,
    {
        fn map<F: Fn(T) -> Result<T, Self::Error>>(
            &self,
            id: &T::Id,
            f: F,
        ) -> Result<Option<T>, Self::Error> {
            let t = self.get_one(id)?;
            if let Some(t) = t {
                let t = f(t)?;
                self.update(t.clone())?;
                Ok(Some(t))
            } else {
                Ok(None)
            }
        }
    }

    impl<T, E, S> RemoveBy<T, S> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
        T::Id: Clone + Eq,
        T: IdentityBy<S>,
        S: PartialEq,
    {
        fn remove_by(&self, id: &S) -> Result<(), Self::Error> {
            let mut cache = self.cache.blocking_lock();
            let res = cache
                .iter()
                .enumerate()
                .find(|(_, (_, v))| &v.id_by() == id)
                .map(|(i, _)| i);
            let Some(i) = res else { return Ok(()) };
            cache.remove(i);
            Ok(())
        }
    }

    impl<T, E> Clear<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
    {
        fn clear(&self) -> Result<(), Self::Error> {
            let mut cache = self.cache.blocking_lock();
            *cache = vec![];
            Ok(())
        }
    }

    impl<T, E> ListPaged<T> for InMemoryRepository<T, E>
    where
        E: Send + 'static,
        T: Identity + Clone,
    {
        fn list_paged(&self, size: usize, page: usize) -> Result<Vec<T>, Self::Error> {
            let cache = self.cache.blocking_lock();
            Ok(cache
                .iter()
                .skip(page.saturating_mul(size))
                .take(size)
                .map(|(_, v)| v)
                .cloned()
                .collect())
        }
    }
}
