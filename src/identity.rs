#![allow(clippy::module_name_repetitions)]

/// Defines type's primary key
///
/// Primary key should be unique, but it is not required
pub trait Identity
where
    Self: Send,
{
    type Id;
}

pub trait GetIdentity: Identity {
    fn id(&self) -> Self::Id;
}

pub trait RefIdentity: Identity {
    fn id_ref(&self) -> &Self::Id;
}

/// Defines additional type's keys
///
/// ```
/// use typesafe_repository::{Identity, IdentityBy};
/// use typesafe_repository::macros::Id;
///
/// type Login = String;
/// type Email = String;
///
/// #[derive(Id)]
/// struct User {
///   #[id]
///   login: Login,
///   #[id_by]
///   email: Email,
/// }
/// ```
pub trait IdentityBy<T> {
    fn id_by(&self) -> T;
}

/// Mark that Selector can be used with type
///
/// This trait is needed to allow using only explicitly specified selectors for type
pub trait SelectBy<S: Selector> {}

/// Marker identity for advanced operations and parameters
/// ```
/// use typesafe_repository::{Identity, GetIdentity, IdentityBy, Repository, Select, Selector,
/// SelectBy};
/// use typesafe_repository::inmemory::InMemoryRepository;
/// use typesafe_repository::macros::Id;
/// use std::error::Error;
/// use std::sync::Arc;
///
/// // ListLatest don't have to contain any information because it's only purpose is to mark that
/// // repository need to return latest products
/// struct ListLatest;
///
/// #[derive(Id)]
/// #[Id(get_id)]
/// struct Product {
///   id: usize,
/// }
///
/// impl Selector for ListLatest {}
/// impl SelectBy<ListLatest> for Product {}
///
/// trait ProductRepository:
///     Repository<Product, Error = Box<dyn Error + Send + Sync>>
///     + Select<Product, ListLatest>
///     {}
///
/// ```
/// In above example, `Select<Product, ListLatest>` implementation could look like:
/// ```
/// # use typesafe_repository::prelude::*;
/// # use typesafe_repository::{Selector, SelectBy, Select};
/// # use typesafe_repository::inmemory::InMemoryRepository;
/// # use typesafe_repository::macros::Id;
/// # use std::error::Error;
/// # use std::sync::Arc;
/// use typesafe_repository::{ListBy, List};
/// use async_trait::async_trait;
/// # struct ListLatest;
/// #
/// # #[derive(Clone, Id)]
/// # #[Id(get_id)]
/// # struct Product {
/// #   id: usize,
/// #   date: usize,
/// # }
/// #
/// # impl Selector for ListLatest {}
/// # impl SelectBy<ListLatest> for Product {}
/// #
/// # struct TestProductRepository {
/// #   storage: InMemoryRepository<Product, Box<dyn Error + Send + Sync>>,
/// # }
/// # impl Repository<Product> for TestProductRepository {
/// #   type Error = Box<dyn Error + Send + Sync>;
/// # }
/// #
///
/// #[async_trait]
/// impl Select<Product, ListLatest> for TestProductRepository {
///   async fn select(&self, _: &ListLatest) -> Result<Vec<Product>, Self::Error> {
///     let mut list = self.storage.list().await?;
///     let len = list.len();
///     list.sort_by_key(|p| p.date);
///     Ok(list.into_iter().skip(len - 5).collect())
///   }
/// }
/// ```
pub trait Selector {}

/// `Identity::Id` shorthand that helps abstract from `Id` type
/// ```
/// use typesafe_repository::{IdentityOf, Identity};
/// use typesafe_repository::macros::Id;
///
/// #[derive(Id)]
/// struct Foo {
///     id: u64,
/// }
/// struct Bar {
///     foo: IdentityOf<Foo>,
/// }
/// ```
pub type IdentityOf<T> = <T as Identity>::Id;

/// Identity which can be obtained only from `impl Identity` or
/// [ValidIdentityProvider](ValidIdentityProvider)
///
/// `ValidIdentity` intended to be used for internal references.  
///
/// It has two main benefits:
/// - Guarantee that identity is valid (points to existing entity)
/// - `ValidIdentity` of `T` cannot be swapped with other entity id by mistake even if
/// `Identity::Id` is the same for two types
///
/// ```
/// use typesafe_repository::{Identity, GetIdentity, ValidIdentity};
/// use typesafe_repository::macros::Id;
///
/// struct Foo {
///   bar: ValidIdentity<Bar>,
/// }
///
/// #[derive(Id)]
/// #[Id(get_id)]
/// struct Bar {
///   id: usize,
/// }
///
/// let bar = Bar { id: 0 };
/// let foo = Foo { bar: ValidIdentity::of(&bar) };
/// ```

/// Trusted source of [ValidIdentity](ValidIdentity)
///
/// Intended to be used in persistence layer or tests in order to construct
/// [ValidIdentity](ValidIdentity) for `T` without actual `T` loaded
///
pub trait ValidIdentityProvider<T>
where
    T: Identity,
{
    fn get(&self) -> IdentityOf<T>;
}

pub struct ValidIdentity<T: Identity>(IdentityOf<T>);

impl<T: Identity + GetIdentity> ValidIdentity<T> {
    /// Construct `ValidIdentity` from `T`
    pub fn of(t: &T) -> Self {
        Self(t.id())
    }
    pub fn into_inner(self) -> IdentityOf<T> {
        self.0
    }
    /// Construct `ValidIdentity` from [ValidIdentityProvider](ValidIdentityProvider)
    pub fn from<P: ValidIdentityProvider<T>>(p: &P) -> Self {
        Self(p.get())
    }
}

impl<T> std::fmt::Debug for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ValidIdentity").field("0", &self.0).finish()
    }
}

impl<T> std::fmt::Display for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: std::fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<T> Clone for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: Clone,
{
    fn clone(&self) -> Self {
        ValidIdentity(self.0.clone())
    }
}

impl<T> PartialEq for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<T> Eq for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: PartialEq,
{
}

use std::cmp::Ordering;

impl<T> PartialOrd for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: PartialOrd,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T> Ord for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: Ord + PartialOrd,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T> std::hash::Hash for ValidIdentity<T>
where
    T: Identity,
    IdentityOf<T>: std::hash::Hash,
{
    fn hash<H>(&self, state: &mut H)
    where
        H: std::hash::Hasher,
    {
        self.0.hash(state);
    }
}

#[cfg(feature = "serde")]
mod serde {
    use super::{Identity, IdentityOf, ValidIdentity};

    use serde::de::{Deserializer, Visitor};
    use serde::ser::Serializer;
    use serde::{Deserialize, Serialize};
    use std::marker::PhantomData;

    impl<T> Serialize for ValidIdentity<T>
    where
        T: Identity + Serialize,
        IdentityOf<T>: Serialize,
    {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            serializer.serialize_newtype_struct("ValidIdentity", &self.0)
        }
    }

    #[doc(hidden)]
    struct ValidIdentityVisitor<T: Identity>(PhantomData<T>);

    impl<'de, T: Identity> Visitor<'de> for ValidIdentityVisitor<T>
    where
        IdentityOf<T>: Deserialize<'de>,
    {
        type Value = ValidIdentity<T>;

        fn expecting(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(formatter, "ValidIdentity")
        }

        fn visit_newtype_struct<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: Deserializer<'de>,
        {
            let t: IdentityOf<T> = Deserialize::deserialize(deserializer)?;
            Ok(ValidIdentity(t))
        }
    }

    impl<'de, T> Deserialize<'de> for ValidIdentity<T>
    where
        T: Identity + Deserialize<'de>,
        IdentityOf<T>: Deserialize<'de>,
    {
        fn deserialize<D>(deserializer: D) -> Result<ValidIdentity<T>, D::Error>
        where
            D: Deserializer<'de>,
        {
            deserializer
                .deserialize_newtype_struct("ValidIdentity", ValidIdentityVisitor(PhantomData))
        }
    }
}

/// Partially initialized entity
///
/// ```
/// use typesafe_repository::macros::Id;
/// use typesafe_repository::{Identity, Dao, Create};
///
/// #[derive(Id)]
/// struct Product {
///   id: usize,
///   name: String,
///   count: usize,
/// }
///
/// struct ProductDao {
///   name: String,
///   count: usize,
/// }
///
/// impl Dao for ProductDao {
///   type Parent = Product;
///   type Args = usize;
///
///   fn enrich(self, id: usize) -> Product {
///     let ProductDao { name, count } = self;
///     Product { id, name, count }
///   }
/// }
/// ```
pub trait Dao {
    type Parent: Identity;
    type Args;

    fn enrich(self, t: Self::Args) -> Self::Parent;
}
