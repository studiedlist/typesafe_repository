//! `typesafe_repository` is designed to abstract data persistence, so its implementations can be
//! easily swapped for testing or migration purposes
//!
//! You can create your own repository by implementing traits like [Add](Add),
//! [List](List), [Get](Get), and plenty of others (see [async_ops](async_ops) for complete list)
//!
//!
//! Typical way to define abstract repository for some type is:
//! ```
//! use typesafe_repository::{Identity, GetIdentity, Repository, Add, List, Remove};
//! use typesafe_repository::inmemory::InMemoryRepository;
//! use typesafe_repository::macros::Id;
//! use std::sync::Arc;
//!
//! #[derive(Clone, Id)]
//! #[Id(get_id)]
//! pub struct Foo {
//!   id: usize,
//! }
//!
//! trait FooRepository:
//!   Repository<Foo, Error = Box<dyn std::error::Error + Send + Sync>>
//!   + Add<Foo>
//!   + List<Foo>
//!   + Remove<Foo>
//! {}
//!
//! impl<T> FooRepository for T
//! where T: Repository<Foo, Error = Box<dyn std::error::Error + Send + Sync>>
//!     + Add<Foo>
//!     + List<Foo>
//!     + Remove<Foo> {}
//!
//! struct FooService {
//!   repo: Arc<dyn FooRepository>,
//! }
//!
//! // You can mock any repository with InMemoryRepository since it implements all operations on
//! // all types
//! let repo = Arc::new(InMemoryRepository::new());
//! FooService { repo };
//! ```
#![deny(
    future_incompatible,
    let_underscore,
    keyword_idents,
    elided_lifetimes_in_paths,
    meta_variable_misuse,
    noop_method_call,
    unused_lifetimes,
    unused_qualifications,
    unsafe_op_in_unsafe_fn,
    clippy::undocumented_unsafe_blocks,
    clippy::wildcard_dependencies,
    clippy::debug_assert_with_mut_call,
    clippy::empty_line_after_outer_attr,
    clippy::panic,
    clippy::unwrap_used,
    clippy::expect_used,
    clippy::redundant_field_names,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::unneeded_field_pattern,
    clippy::useless_let_if_seq,
    clippy::default_union_representation,
    clippy::arithmetic_side_effects,
    clippy::checked_conversions,
    clippy::dbg_macro
)]
#![warn(
    clippy::pedantic,
    clippy::cargo,
    clippy::cloned_instead_of_copied,
    clippy::cognitive_complexity
)]
#![allow(
    clippy::must_use_candidate,
    clippy::doc_markdown,
    clippy::missing_errors_doc,
    clippy::wildcard_imports
)]

#[cfg(feature = "macros")]
pub mod macros {
    pub use typesafe_repository_macro::Id;
}

pub mod prelude {
    pub use super::{
        identity::{GetIdentity, Identity, IdentityBy, IdentityOf, RefIdentity},
        Repository,
    };
}

#[cfg(feature = "inmemory")]
pub mod inmemory;

pub mod identity;

pub use identity::*;

/// Transactional traits and wrapper for [Repository](Repository)
///
/// [RepositoryWrapper](transactional::RepositoryWrapper)
#[cfg(feature = "transactional")]
pub mod transactional;

/// Marker trait with `Error` definition for operations
///
pub trait Repository<V: Identity> {
    type Error: Send + 'static;
}

#[cfg(feature = "async")]
pub use async_ops::*;
#[cfg(not(feature = "async"))]
pub use ops::*;

macro_rules! asyncify {
    (
        $($import:item);*
        $(#[doc = $mod_doc:expr])*
        mod $mod_name:ident
        $(
            $(#[doc = $doc:expr])*
            pub trait $trait:ident$(<$($gen:tt $( : $con:path )?),*>)?$(: $super:path)?
            $(where
                $($b:path: $($(+ $([$($_:tt)* $optional:tt])?)? $wh_gen:path)*,)*
            )?
            {
                $($func:item);*
            }
        )*
    ) => {
        $(#[doc = $mod_doc])*
        pub mod $mod_name {
            $($import);*
            $(
                $(#[doc= $doc])*
                pub trait $trait$(<$($gen $(: $con)?),*>)?$(: $super)?
                $(where
                    $($b: $($(+ $($optional)?)? $wh_gen)*,)*
                )?
                {
                    $($func);*
                }
            )*
        }
        #[cfg(feature="async")]
        concat_idents::concat_idents!(mod_name = async_, $mod_name {
        pub mod mod_name {
            $($import);*
            use async_trait::async_trait;

            $(
                #[async_trait]
                pub trait $trait$(<$($gen $(: $con)?),*>)?$(: $super)?
                $(where
                    $($b: $($(+ $($optional)?)? $wh_gen)*,)*
                )?
                {
                    async $($func);*
                }
            )*

        }
        });
    }
}

asyncify! {
    use crate::*;

    /// operations for [Repository](Repository)
    ///
    ///```
    ///  use typesafe_repository::ops::Add;
    ///  use typesafe_repository::{Repository, Identity, RefIdentity, GetIdentity};
    ///  use typesafe_repository::macros::Id;
    ///  use std::sync::Mutex;
    ///  use std::error::Error;
    ///  use std::sync::Arc;
    ///
    ///  #[derive(Id, Debug, Clone, PartialEq)]
    ///  #[Id(get_id, ref_id)]
    ///  pub struct Foo {
    ///    id: usize,
    ///  }
    ///
    ///  pub struct ExampleRepository {
    ///     foos: Arc<Mutex<Vec<Foo>>>,
    ///  }
    ///
    ///  impl Repository<Foo> for ExampleRepository {
    ///     type Error = Box<dyn Error + Send + 'static>;
    ///  }
    ///
    ///  impl Add<Foo> for ExampleRepository {
    ///     fn add(&self, f: Foo) -> Result<(), Self::Error> {
    ///         self.foos.lock().unwrap().push(f);
    ///         Ok(())
    ///     }
    ///  }
    ///
    ///  #[tokio::main]
    ///  async fn main() {
    ///     let foos = Arc::new(Mutex::new(vec![]));
    ///     let repo = ExampleRepository { foos: foos.clone() };
    ///     let foo = Foo { id: 10 };
    ///     assert_eq!((), repo.add(foo.clone()).unwrap());
    ///     assert_eq!(Some(foo.clone()), foos.lock().unwrap().pop());
    ///  }
    ///```
    mod ops

    /// Generate `id` for [DAO](DAO) of `V`, and persist [DAO](Dao)
    ///
    /// This trait is usable when `id` cannot be generated in-place (like sequential database id)
    pub trait Create<V: Identity, D: Dao<Parent = V>>: Repository<V> {
        fn create(&self, d: D) -> Result<IdentityOf<V>, Self::Error>;
    }

    /// Add entity to repository
    pub trait Add<V: Identity>: Repository<V> {
        fn add(&self, v: V) -> Result<(), Self::Error>;
    }

    /// List all entities in repository
    pub trait List<V: Identity>: Repository<V> {
        fn list(&self) -> Result<Vec<V>, Self::Error>;
    }

    /// Get one entity by primary key
    pub trait Get<V: Identity>: Repository<V> {
        fn get_one(&self, k: &V::Id) -> Result<Option<V>, Self::Error>;
    }

    /// Acts as either [Add](Add) or [Update](Update)
    pub trait Save<V: Identity>: Repository<V> {
        fn save(&self, v: V) -> Result<(), Self::Error>;
    }

    /// Update entity identified by primary key in repository
    pub trait Update<V: Identity>: Repository<V> {
        fn update(&self, v: V) -> Result<(), Self::Error>;
    }

    /// Remove entity from repository by primary key
    pub trait Remove<V: Identity>: Repository<V> {
        fn remove(&self, k: &V::Id) -> Result<(), Self::Error>;
    }

    /// Remove entity from repository by value
    pub trait RemoveByValue<V: Identity>: Repository<V> {
        fn remove(&self, v: V) -> Result<(), Self::Error>;
    }

    /// [Get](Get) and [Remove](Remove) entity by primary key
    pub trait Take<V: Identity>: Repository<V> {
        fn take(&self, k: &V::Id) -> Result<Option<V>, Self::Error>;
    }

    /// Check if entity exists by primary key
    pub trait Exists<V: Identity>: Repository<V> {
        fn exists(&self, k: &V::Id) -> Result<bool, Self::Error>;
    }

    /// Find one entity by secondary key ([IdentityBy](IdentityBy) trait)
    pub trait GetBy<V, S>: Repository<V>
    where
        V: IdentityBy<S> + Identity,
    {
        fn get_by(&self, s: &S) -> Result<Option<V>, Self::Error>;
    }

    /// Find many entities by secondary key ([IdentityBy](IdentityBy) trait)
    pub trait ListBy<V, S>: Repository<V>
    where
        V: IdentityBy<S> + Identity,
    {
        fn list_by(&self, s: &S) -> Result<Vec<V>, Self::Error>;
    }

    /// Remove many entities by secondary key ([IdentityBy](IdentityBy) trait)
    pub trait RemoveBy<V, S>: Repository<V>
    where
        V: IdentityBy<S> + Identity,
    {
        fn remove_by(&self, s: &S) -> Result<(), Self::Error>;
    }

    /// Count entities by secondary key ([IdentityBy](IdentityBy) trait)
    pub trait Count<V, S>: Repository<V>
    where
        V: IdentityBy<S> + Identity,
    {
        fn count(&self, s: &S) -> Result<(), Self::Error>;
    }

    /// List entities by pages of given `size` starting from `page`
    pub trait ListPaged<V>: Repository<V>
    where
        V: Identity,
    {
        fn list_paged(&self, size: usize, page: usize) -> Result<Vec<V>, Self::Error>;
    }

    /// Find many entities by selector ([SelectBy](SelectBy) trait)
    pub trait Select<V, S>: Repository<V>
    where
        V: Identity + SelectBy<S>,
        S: Selector,
    {
        fn select(&self, s: &S) -> Result<Vec<V>, Self::Error>;
    }

    /// Find one entity by selector ([SelectBy](SelectBy) trait)
    pub trait Find<V, S>: Repository<V>
    where
        V: Identity + SelectBy<S>,
        S: Selector,
    {
        fn find(&self, s: &S) -> Result<Option<V>, Self::Error>;
    }

    /// Map entity by primary key
    pub trait Map<V>: Repository<V>
    where
        V: Identity,
    {
        fn map<F: Fn(V) -> Result<V, Self::Error> + Send>(
            &self,
            id: &IdentityOf<V>,
            f: F
        ) -> Result<Option<V>, Self::Error>;
    }

    /// Clear repository
    pub trait Clear<V>: Repository<V>
    where
        V: Identity,
    {
        fn clear(&self) -> Result<(), Self::Error>;
    }
}
