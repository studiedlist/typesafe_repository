# Features
- Identity traits (plus helper types like `ValidIdentity` and `IdentityOf`)
- Repository traits like `Add`, `Get`, `List` 
- No need to use `unimplemented!()` or blanket implementations- construct your own repository based on set of traits
- No heavy macro magic
- Transactional repository traits based on `HCons` with rollback feature 
- Transactional wrapper for repository, which can be used both as plain repository 
  inheriting behavior of inner repository, or transactional repository
- Perform operations on many repositories in one transaction

# Goals
- Provide flexible and abstract generic-based repository traits
- Easy transaction implementation
- Non-essential features are optional
- No unsafe and panics
- No runtime errors caused by library
- No enforced error types

# Roadmap
- [x] transactional traits and `RepositoryWrapper`
- [x] `RepositoryWrapper` rollback support
- [x] seamless transactional and non-transactional interop for `RepositoryWrapper`
- [x] `IdentityBy`-like trait for marker structs
- [x] optional serde support
- [x] sync/async operations with optional features
- [ ] sync transactions support
- [ ] pointer-based `ValidIdentity`
- [ ] pass previous operation result as next operation argument in transactions
- [ ] `HCons` wrapper for more convenient output extraction
- [ ] `TransactionRunner` settings and different mappers
- [ ] transaction log
- [ ] trait generation based on methods names (like in Spring repositories)


# Notes
Many generic bounds were made out of heuristics, so feel free to propose reasonable bound changes

Project is in early stage, expect breaking API changes
